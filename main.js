const { app, BrowserWindow } = require('electron');
const path = require('path');

function createWindow(file) {
  const mainWindow = new BrowserWindow({
    width: 800,
    height: 600,
    webPreferences: {
      nodeIntegration: true // Enable Node.js integration if needed
    }
  });

  mainWindow.loadFile(file);
}

app.whenReady().then(() => {
  createWindow('index.html');

  app.on('activate', function () {
    if (BrowserWindow.getAllWindows().length === 0) createWindow();
  });
});

app.on('window-all-closed', function () {
  if (process.platform !== 'darwin') app.quit();
});
