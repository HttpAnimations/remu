const { dialog } = require('electron').remote;
const fs = require('fs');
const path = require('path');

const selectFolderBtn = document.getElementById('selectFolderBtn');

selectFolderBtn.addEventListener('click', async () => {
  const result = await dialog.showOpenDialog({
    properties: ['openDirectory']
  });

  if (!result.canceled && result.filePaths.length > 0) {
    const selectedFolder = result.filePaths[0];
    saveFolder(selectedFolder);
  }
});

function saveFolder(folderPath) {
  const configFilePath = path.join(__dirname, 'files.yml');
  const configData = { musicFolder: folderPath };

  fs.writeFileSync(configFilePath, YAML.stringify(configData), 'utf8');
}
