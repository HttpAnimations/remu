const fs = require('fs');
const path = require('path');
const YAML = require('yaml');

document.addEventListener('DOMContentLoaded', async (event) => {
  const audioPlayer = document.getElementById('audioPlayer');
  const fileInput = document.getElementById('fileInput');
  const settingsBtn = document.getElementById('settingsBtn');
  const musicList = document.getElementById('musicList');

  // Load music files from config
  const configFilePath = path.join(__dirname, 'files.yml');
  const configData = fs.readFileSync(configFilePath, 'utf8');
  const config = YAML.parse(configData);
  const musicFolder = config.musicFolder;

  // Display music files
  if (musicFolder) {
    const files = await loadMusicFiles(musicFolder);
    displayMusicFiles(files);
  }

  // Event listener for settings button
  settingsBtn.addEventListener('click', () => {
    window.location.href = 'settings.html';
  });

  // Event listener for selecting a file
  fileInput.addEventListener('change', function(event) {
    const filePath = event.target.files[0].path;

    // Pause the audio if it's currently playing
    if (!audioPlayer.paused) {
      audioPlayer.pause();
    }

    // Set the new source
    audioPlayer.src = filePath;

    // Wait for the canplay event before playing
    audioPlayer.addEventListener('canplay', function() {
      audioPlayer.play();
    });
  });

  // Function to load music files from a directory
  async function loadMusicFiles(folderPath) {
    try {
      const files = await fs.promises.readdir(folderPath);
      return files.filter(file => file.endsWith('.mp3') || file.endsWith('.wav'));
    } catch (error) {
      console.error('Error loading music files:', error);
      return [];
    }
  }

  // Function to display music files in the UI
  function displayMusicFiles(files) {
    musicList.innerHTML = '';
    files.forEach(file => {
      const li = document.createElement('li');
      li.textContent = file;
      li.addEventListener('click', () => {
        const filePath = path.join(musicFolder, file);
        audioPlayer.src = filePath;
        audioPlayer.play();
      });
      musicList.appendChild(li);
    });
  }
});
